<?php

namespace App\Tests\Functional;

use App\Repository\ItemRepository;
use Nzo\UrlEncryptorBundle\Encryptor\Encryptor;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class ItemControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);
        
        $data = 'very secure new item data';

        $newItemData = ['data' => $data];

        $client->request('POST', '/item', $newItemData);
        $client->request('GET', '/item');

        self::assertResponseIsSuccessful();
        $this->assertStringContainsString($data, $client->getResponse()->getContent());
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);
        $itemRepository = static::$container->get(ItemRepository::class);
        $enc = static::$container->get(Encryptor::class);

        $user = $userRepository->findOneByUsername('john');
        $item = $itemRepository->findOneByUser($user);
        $client->loginUser($user);

        $decrypted = $enc->decrypt($item->getData());
        $data = 'Changed user data';
        $newItemData = [
            'data' => $data,
            'id' => $item->getId(),
        ];

        $client->request('PUT', '/item', $newItemData);
        $client->request('GET', '/item');

        self::assertResponseIsSuccessful();
        $this->assertStringNotContainsString($decrypted, $client->getResponse()->getContent());
        $this->assertStringContainsString($data, $client->getResponse()->getContent());
    }

    public function testDelete()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);
        $itemRepository = static::$container->get(ItemRepository::class);

        $user = $userRepository->findOneByUsername('john');
        $item = $itemRepository->findOneByUser($user);

        $dataToDelete = $item->getData();

        $client->loginUser($user);

        $client->request('DELETE', '/item/' . $item->getId());
        $client->request('GET', '/item');

        self::assertResponseIsSuccessful();
        $this->assertStringNotContainsString($dataToDelete, $client->getResponse()->getContent());
    }
}
